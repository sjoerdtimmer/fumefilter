
$fn=30;

use <corners.scad>



difference(){
	translate([-6,-0.6,0])
	linear_extrude(height=0.3)
	rounded_square([12,1.2],[0.3,0.3,0.3,0.3]);

	translate([5.25,0,-3])
	cylinder(r=0.25,h=4);
	translate([5.25,0,0.2])
	cylinder(r1=0.25,r2=0.3,h=0.1001);

	translate([-5.25,0,-3])
	cylinder(r=0.25,h=4);
	translate([-5.25,0,0.2])
	cylinder(r1=0.25,r2=0.3,h=0.1001);

	for(x = [-4:4]){
		translate([x,0,-3])
		cylinder(r=0.25,h=4);
		
		translate([x,0,0.2501])
		cylinder(r=0.3,h=0.05);

		translate([x,0,0.2])
		cylinder(r1=0.25,r2=0.3,h=0.0501);
	}

	translate([-5,-1.5,-0.1])
	linear_extrude(height=0.5)
	rounded_square([10,1],[0.3,0.3,0.3,0.3]);	

	translate([-5,0.5,-0.1])
	linear_extrude(height=0.5)
	rounded_square([10,1],[0.3,0.3,0.3,0.3]);	
}

