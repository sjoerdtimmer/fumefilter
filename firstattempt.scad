module hexgrid(size,height,cols,rows){
	for(x = [0:rows]){
		for(y = [0:cols]){
			translate([1.75*size*x,2*y*size+(x%2)*size,-height/2])
			cylinder(h=height,r=size,$fn=6);
		}
	}
}

module ratriangle(size) {
	rotate([90,0,90])
linear_extrude(height=1)
polygon(points=[[0,0],[0,size],[size,0]], paths=[[0,1,2]]);
}


module hexmesh(){
difference(){
	cube([0.2,12,12],center=true);
	translate([0,-6,6.3])
	rotate([0,90,0])
	hexgrid(0.3,0.5,21,25);
}
}


/*=== fan ===*/
translate([0.4-1.24,0,0])
rotate([90,45,90])
color([0.2,0.2,0.2])
import("fan.stl");


/*=== mesh ===*/
translate([1,0,0])
hexmesh();

/*=== front border ===*/
translate([1-0.1,-6,-6])
difference(){
	cube([0.2,12,12]);
	translate([-0.1,0.25,0])
	cube([0.4,11.5,11.75]);
}

/*=== base plate ===*/
translate([-6+1+0.1,-6,-6-0.5])
cube([6,12,0.5]);


/*=== foots to keep stuff in place ===*/
translate([1,6,-6])
rotate([0,0,180])
ratriangle(1);

translate([0,-6,-6])
ratriangle(1);



difference(){
	translate([-2.5,6,-6])
	rotate([0,0,180])
	ratriangle(2);
	
	translate([-4,5.5,-5.5])
	rotate([0,90,0])
	cylinder(r=0.25,h=2,$fn=15);
}


difference(){
	translate([-3.5,-6,-6])
	ratriangle(2);

	translate([-4,-5.5,-5.5])
	rotate([0,90,0])
	cylinder(r=0.25,h=2,$fn=15);
}