use <corners.scad>
use <screwholes.scad>

$fn=60;
inch = 2.54;
radius = 6/cos(22.5);

module frontpart(){


	/* stand on the front */
	color([0.18,0.71,0.84])
	translate([0,0,-6])
	intersection(){
	translate([-1,0,0])
	rotate([0,0,30])
	cylinder(r=4.5/cos(22.5),h=0.3,$fn=6);
	translate([0,-6,0])
	cube([6,12,0.3]);
	}


	module hexagon(r,h){
				cylinder(h=h,r=r,$fn=6);
	}

	/* create an area that covers at least width*height around the origin
	   the area is filled with hexagons with radius r separated by dist from each other. */
	module hexarea(width,height,h,dist,r){
		/* these are upper estimates of how many hexagons may be required for this area: */
		for(x = [ceil(-width/r/2):ceil(width/r/2)],y = [-ceil(height/(sqrt(3)*r)/2):ceil(height/(sqrt(3)*r)/2)]){
			translate([1.5*(r+dist)*x,(r+dist)*(y+(x%2)/2)*sqrt(3)])
			hexagon(r,h);
		}
	}

	/* create an area that covers at least width*height around the origin
	   the area is filled with hexagons with radius r separated by dist from each other. */
	module hexcirc(rad,h,dist,r){
		/* these are upper estimates of how many hexagons may be required for this area: */
		for(x = [ceil(-rad/r):ceil(rad/r)],y = [-ceil(rad/(sqrt(3)*r)):ceil(rad/(sqrt(3)*r))]){
			assign(
			posx = 1.5*(r+dist)*x,
			posy = (r+dist)*(y+(x%2)/2)*sqrt(3)){
				if(posx*posx+posy*posy < rad*rad){
					translate([posx,posy])
					hexagon(r,h);
				}
			}
		}
	}



	color([0.18,0.71,0.84])
	rotate([0,90,0])
	difference(){
		translate([-6,-6,0])
	//	cube([12,12,0.3]);
		linear_extrude(height=0.3)
		rounded_square([12,12],[0.3,0.3,0.3,0.3]);

		translate([0,0,-0.25])
		/* subtract raster */
		hexcirc(5.5,1,0.08,1.25);
		/* subtract screw holes */
		for(x=[-5.25,5.25],y=[-5.25,5.25]){
			translate([x,y,0])
			myfanscrew();
		}
	}




	//color("green")
	color([0.18,0.71,0.84])
	translate([0.3,0,0])
	rotate([22.5,0,0])
	rotate([0,90,0])
	difference(){
		cylinder(r=radius,h=0.25*inch-0.1,$fn=8);
		translate([0,0,-0.1])
		cylinder(r=radius-0.3,h=0.25*inch+0.2,$fn=8);
	}


	//color("red")
	color([0.18,0.71,0.84])
	translate([0.3+0.25*inch-0.1,0,0])
	rotate([22.5,0,0])
	rotate([0,90,0])
	difference(){
		cylinder(r=radius,h=0.3,$fn=8);
		translate([0,0,-0.1])
		cylinder(r=radius-0.6,h=0.5,$fn=8);

		cylinder(r1=radius-0.3,r2=radius-0.6,h=0.1,$fn=8);
	}



}

rotate([0,-90,0])
frontpart();