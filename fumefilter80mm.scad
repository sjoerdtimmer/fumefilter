
inch = 2.54;

module hexagon(r,h){
			cylinder(h=h,r=r,$fn=6);
}

/* create an area that covers at least width*height around the origin
   the area is filled with hexagons with radius r separated by dist from each other. */
module hexarea(width,height,h,dist,r){
	/* these are upper estimates of how many hexagons may be required for this area: */
	for(x = [ceil(-width/r/2):ceil(width/r/2)],y = [-ceil(height/(sqrt(3)*r)/2):ceil(height/(sqrt(3)*r)/2)]){
		translate([1.5*(r+dist)*x,(r+dist)*(y+(x%2)/2)*sqrt(3)])
		hexagon(r,h);
	}
}

/* create an area that covers at least width*height around the origin
   the area is filled with hexagons with radius r separated by dist from each other. */
module hexcirc(rad,h,dist,r){
	/* these are upper estimates of how many hexagons may be required for this area: */
	for(x = [ceil(-rad/r):ceil(rad/r)],y = [-ceil(rad/(sqrt(3)*r)):ceil(rad/(sqrt(3)*r))]){
		assign(
		posx = 1.5*(r+dist)*x,
		posy = (r+dist)*(y+(x%2)/2)*sqrt(3)){
			if(posx*posx+posy*posy < rad*rad){
				translate([posx,posy])
				hexagon(r,h);
			}
			else{
				//color("red")
				//translate([1.5*(r+dist)*x,(r+dist)*(y+(x%2)/2)*sqrt(3)])
				//hexagon(r,h);
			}
		}
	}
}


/* fan */

translate([4.5,1.6-4,1.6+4])
rotate([0,0,90])
color([0.2,0.2,0.2])
import("80mm.stl");


/*
translate([0,-6,-6])
resize([2.5,12,12])
rotate([0,0,90])
color([0.2,0.2,0.2])
import("fan2.stl");
*/



color([0.18,0.71,0.84])
rotate([0,90,0])
difference(){
	translate([-4,-4,0])
	cube([8,8,0.3]);

	translate([0,0,-0.25])
	hexcirc(3.5,1,0.1,0.8);
	/* subtract screw holes */
	translate([3.6,3.6,-3])
	cylinder(r=0.2,h=4,$fn=15);
	translate([3.6,-3.6,-3])
	cylinder(r=0.2,h=4,$fn=15);
	translate([-3.6,3.6,-3])
	cylinder(r=0.2,h=4,$fn=15);
	translate([-3.6,-3.6,-3])
	cylinder(r=0.2,h=4,$fn=15);
}




radius = 4/cos(22.5);


color([0.18,0.71,0.84])
translate([0.3,0,0])
rotate([22.5,0,0])
rotate([0,90,0])
difference(){
	cylinder(r=radius,h=0.25*inch,$fn=8);
	translate([0,0,-0.1])
	cylinder(r=radius-0.3,h=0.25*inch+0.2,$fn=8);
}



color([0.18,0.71,0.84])
translate([0.3+0.25*inch,0,0])
rotate([22.5,0,0])
rotate([0,90,0])
difference(){
	cylinder(r=radius,h=0.3,$fn=8);
	translate([0,0,-0.1])
	cylinder(r=radius-0.6,h=0.5,$fn=8);
}


/* the filter */
color([0.5,0.5,0.5,0])
translate([0.3,0,0])
rotate([22.5,0,0])
rotate([0,90,0])
cylinder(r=radius-0.4,h=0.3,$fn=8);

