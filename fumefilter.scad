
 
use <fumefilter-frontpart.scad>
use <fumefilter-backpart.scad>



//* // fan 
translate([-1,0,0])
rotate([90,45,90])
color([0.2,0.2,0.2])
import("fan.stl");
//*/

/* // alternative fan
translate([0,-6,-6])
resize([2.5,12,12])
rotate([0,0,90])
color([0.2,0.2,0.2])
import("fan2.stl");
//*/


//* // the filter 
color([0.2,0.2,0.2,0.9])
translate([0.3+0.001,0,0.00])
rotate([22.5,0,0])
rotate([0,90,0])
cylinder(r= 6/cos(22.5)-0.4,h=0.25*2.54-0.002,$fn=8);
//*/
 
frontpart();


backpart();

