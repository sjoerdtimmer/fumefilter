
use <corners.scad>
use <screwholes.scad>

$fn=60;

/* stands on the back: */
module backpart(){
	inch = 2.54;
	radius = 6/cos(22.5);

	color([0.18,0.71,0.84])
	translate([-2.5-0.2,0,0])
	union(){
		

		rotate([0,-90,0])
		difference(){
			intersection(){	
				translate([-6,-6,0])
				//cube([6,12,0.3]);
				linear_extrude(height=0.3)
				rounded_square([6,12],[0.3,0.3,0.3,0.3]);
				union(){
					translate([-7,-5.5,0])
					rotate([0,0,30])
					cylinder(h=0.3,r=3,$fn=6);		
					translate([-7,5.5,0])
					rotate([0,0,30])
					cylinder(h=0.3,r=3,$fn=6);		
				}
		}
		
			/* subtract screw holes */
			for(x=[-5.25,5.25],y=[-5.25,5.25]){
				translate([x,y,0])
				myfanscrew();
			}
		}

		/* foot */
		translate([0,0,-6])
		intersection(){
			rotate([0,0,30])
			cylinder(r=4.5/cos(22.5),h=0.3,$fn=6);
			translate([-6,-6,0])
			cube([6,12,0.3]);
		}
	}
}

translate([0,0,0.3])
rotate([0,90,0])
translate([3,0,6])
backpart();