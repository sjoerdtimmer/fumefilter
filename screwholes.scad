$fn=100;

use <corners.scad>

module screwhole(r1,r2,h1,h2,h3,overhang=0.0001){
	translate([0,0,-overhang])
	cylinder(r=r1,h=h1+2*overhang);

	translate([0,0,h1])
	cylinder(r1=r1,r2=r2,h=h2);

	translate([0,0,h1+h2-overhang])
	cylinder(r=r2,h=h3+2*overhang);

}


module myfanscrew(overhang=0.0001){
	screwhole(r1=0.27,r2=0.34,h1=0.3-0.1,h2=0.05,h3=0.05,overhang=overhang);	
}
 
/* testpiece  uses radius 0.30, was too big */
/* testpiece2 uses radius 0.25, was too small */

// as a standalone file print a testpiece:
translate([-2.5,-0.5,0])
difference(){
	translate([-0.5,-0.5,0])
	//cube([6,2,0.3]);
	linear_extrude(height=0.3)
	rounded_square([6,2],[0.3,0.3,0.3,0.3]);

	for(x=[0:5]){
		translate([x,0,0])
		screwhole(r1=0.25+x*0.01,r2=0.4,h1=0.3-0.1,h2=0.05,h3=0.05);	
		color([1,0,0])
		translate([x,0,0.5+0.3-0.05])
		scale([0.13,0.13,1])
		rotate([0,0,50])		
		writecircle(text=str("r=",10*(0.25+0.01*x)),h=1,radius=4);

		translate([x,1,0])
		screwhole(r1=0.29,r2=0.4,h1=0.3-0.05-0.01*x,h2=0.0,h3=0.05+0.01*x);	
		color([0,0,1])
		translate([x,1,0.5+0.3-0.05])
		scale([0.13,0.13,1])
		rotate([0,0,-130])		
		writecircle(text=str("d=",10*(0.05+0.01*x)),h=1,radius=4);

	}


	
}



