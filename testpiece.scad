/* This is a smell testpiece to test some different things since I am uncertain of the 3d printer i will be using. The most important is the overhang. When printing the filter part, a small piece of the octogonal frame will hang over to keep the filter in position.

The second is the shape and position of the screwholes. I have added a screwhole on the corner at the same distance of that corner as in the actual model. In need to see if the corner remains string enough and if the distance to the corner is allright. I am also not certain that the diameter is correct. Also, I have added a little room for the screwhead to sink into the plastic. The dismensions of the sink are just guesses so I will need to see how that works out...

Thirdly, there is a small part of the hexagongrid attached to test of such a structure prints correctly and if it will be strong enough...
*/


$fn=150;
inch = 2.54;

use <fumefilter.scad>
use <corners.scad>

color([0,0.5,1])
difference(){
	translate([-1,-1,0])
	linear_extrude(height=0.3)
	rounded_square([3,5.5],[0.3,0.3,0.3,0.3]);
	//cube([3,5.5,0.3]);

	/* hexagons */
	translate([-1.5,6,-0.05])
	hexcirc(5.5,1,0.08,1.25);

	/* first hole */
	translate([-0.5,-0.5,0]){
		/* screw shaft */
		translate([0,0,-0.1])
		cylinder(r=0.3,h=0.4);
		/* screw head */
		translate([0,0,0.20])
		cylinder(r1=0.25,r2=0.4,h=0.1001);
	}

	/* second hole: */
	translate([0.7,0,0]){
		/* screw shaft */
		translate([0,0,-0.1])
		cylinder(r=0.3,h=0.4);
		/* screw head */
		translate([0,0,0.20])
		cylinder(r1=0.25,r2=0.4,h=0.1001);
	}

}

color([0,0.5,1])
translate([1.5,-0.5,0.3])	
cube([0.3,2,0.25*inch]);

color([0,0.5,1])
translate([1.5,-0.5,0.3+0.25*inch])
cube([0.6,2,0.3]);
	



